import { createSlice } from "@reduxjs/toolkit";
import { PA } from "utils/modules";






const compSlice = createSlice({
  name: 'comp',
  initialState: [{
    text: ''
  }],
  reducers:{
    addValue: 
      (state, action: PA) =>  [ ...state, action.payload]
  }
})


export const {addValue} = compSlice.actions

export default compSlice 
