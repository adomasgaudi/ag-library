import { configureStore } from "@reduxjs/toolkit";
import compSlice from "./comp.slice";

export const store = configureStore({
  reducer:{
    comp: compSlice.reducer
  }
})





