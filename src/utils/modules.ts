import { PayloadAction } from "@reduxjs/toolkit";

export interface objType {
  text: string
}

export type PA = PayloadAction<objType>