import { ContainerSC, Nav } from "comps";
import React from "react";
import { WelcomeList } from "./WelcomeList";


export const Welcome = () => {
  

  return (
    <div>
      <Nav/>
      <ContainerSC>
        <h1>Welcome to the AG library:</h1>
        <p>
          in here i showcase a structured commits of all of the features that i
          use in a react project
        </p>
        <h3>featured technologies include: </h3>
        
        <WelcomeList/>
      </ContainerSC>
    </div>
  );
};
