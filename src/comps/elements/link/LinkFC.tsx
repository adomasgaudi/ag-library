import React from 'react'
import { LinkFC__styled } from './LinkFC.styled'

export const LinkFC: React.FC<{href: string, col?: string | undefined}> = ({children, href, col}) => {
  return (
    <>
      <LinkFC__styled target="_blank" href={href} coll={col}>{children}</LinkFC__styled>
    </>
  )
}
